# Hi 👋, I'm [Mukesh](https://github.com/hakxcore) <img src="media/butterfly.gif" alt="butterfly" width="150"/> <img src="media/dog.gif" alt="butterfly" width="200"/>
<!--<h3 align="center"><img src="media/animated-tree.gif" alt="butterfly" width=15%/></h>-->
</br>
</br>
</br>

<table style="border: none;">
  <tr style="border: none; padding: 0; margin: 0;">
  <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://github.com/ryo-ma/github-profile-trophy" target="_blank">
      <img src="https://github-profile-trophy.vercel.app/?username=hakxcore" alt="hakxcore" width='700' height='200' style='margin-right: 1rem;'/>
    </a>
    </td>
  </tr>
  </table>
<p align="left"> <img src="https://komarev.com/ghpvc/?username=hakxcore&label=Profile%20views&color=0e75b6&style=flat" alt="hakxcore" /> </p>

- 🔭 I’m currently working on [`fig`](https://github.com/hakxcore/fig) project

- 🌱 I’m currently learning **`Go`**

- 👯 I’m looking to collaborate on [`Termux-snippets`](https://github.com/hakxcore/Termux-snippets)

- 🤝 I’m looking for help with [`TrackPhoneNumber`](https://github.com/hakxcore/TrackPhoneNumber)

- 💬 Ask me about **`pentesting`**

- 📫 How to find me **from 👇** 

<h3 align="left">Connect with me:</h3>


<table style="border: none;">
  <tr style="border: none; padding: 0; margin: 0;">
 
  <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://dev.to/hakxcore" target="_blank">
      <img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/dev-dot-to.svg" alt="Dev hakxcore" width='30' height='40' style='margin-right: 1rem;'/>
    </a>
   </td>
    
    
  <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://twitter.com/hakxcore" target="_blank">
      <img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="hakxcore twitter" width='30' height='40' style='margin-right: 1rem;'/>
    </a>
   </td>
    
    
  <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://www.facebook.com/profile.php?id=100020452812971)" target="_blank">
      <img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/facebook.svg" alt="Mukesh kumar's facebook" width='30' height='40' style='margin-right: 1rem;'/>
    </a>
   </td>
    
  <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://www.instagram.com/hakxcore/" target="_blank">
      <img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="Mukesh kumar's instagram" width='30' height='40' style='margin-right: 1rem;'/>
    </a>
   </td>
 
   <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://www.youtube.com/channel/UCmN1GRJ76H4y45Y81iJbYWQ" target="_blank">
      <img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/youtube.svg" alt="Youtube Hakxcore" width='30' height='40' style='margin-right: 1rem;'/>
    </a>
   </td
    </tr>
</table>
    

<h3 align="left">Languages and Tools I Know:</h3>

<table style="border: none;">
  <tr style="border: none; padding: 0; margin: 0;">
 
  <td style="border: none; padding: 0; margin: 0;"> 
    <a href="https://www.gnu.org/software/bash/" target="_blank">
      <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width='40' height='40' style='margin-right: 1rem;'/>
    </a>
   </td>
  
   <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://www.cprogramming.com/" target="_blank"> 
    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
     </td>
     
  <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://www.w3schools.com/cpp/" target="_blank"> 
    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width='40' height='40' style='margin-right: 1rem;'/> 
  </a> 
    </td>        
  </a>
       </td>      
             <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://www.figma.com/" target="_blank"> 
    <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
       </td>        
               <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://firebase.google.com/" target="_blank"> 
    <img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
     </td>            
  <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://git-scm.com/" target="_blank"> 
    <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
    </td>
      <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> 
    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
    </td>    
  <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://www.linux.org/" target="_blank"> 
    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
    </td>
 <td style="border: none; padding: 0; margin: 0;"> 
  <a href="https://www.python.org" target="_blank"> 
    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width='40' height='40' style='margin-right: 1rem;'/> 
  </a>
   </td>
    </tr>
</table>


<h3 align="left">Github Stats:</h3>

<p align="center"><img src="https://github-readme-stats.vercel.app/api?username=hakxcore&theme=github_dark&count_private=true&show_icons=true&include_all_commits=true&hide_border=true" width="495"></p>

---

<p align="center"><img src="http://github-readme-streak-stats.herokuapp.com?user=hakxcore&theme=github-dark-blue&hide_border=true" width="495"></p>

---

<p align="center"><img src="https://wakatime.com/share/@6cfc6d05-38b2-48d6-83aa-2609e431c00d/fbc86a23-f5c5-48b0-b0b1-f6067ceb44c4.svg" width="495"></p>

<p></p>

[![Hakxcor's GitHub Activity Graph](https://activity-graph.herokuapp.com/graph?username=hakxcore&theme=xcode)](https://activity-graph.herokuapp.com/graph?username=hakxcore&theme=xcode)
  <br/> <br/> 

[comment]: <> (
hakxcore
resources
investigator
jktravels
investigation
domain-investigation
music
clock
sunrise
aday
morph
audio
Matrix
glitch
weather
idcard
business
links-sites
nonprofit-org
)

<h3 align="left">Support:</h3>
<p><a href="https://www.buymeacoffee.com/hakxcore"> <img align="left" src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="210" alt="https://www.buymeacoffee.com/hakxcore" /></a></p><br/><br/>

<h3 align="left">Badges:</h3>
<a href='https://docs.github.com/en/developers'><img src='https://raw.githubusercontent.com/acervenky/animated-github-badges/master/assets/devbadge.gif' width='40' height='40'>
</a> <a href='https://education.github.com/pack'><img src='https://raw.githubusercontent.com/acervenky/animated-github-badges/master/assets/pro.gif' width='40' height='40'></a>
<br /> <br /> 
